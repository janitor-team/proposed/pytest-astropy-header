0.1.2 (2019-12-18)
==================

- Handle the case where the astropy version is 'unknown'. [#11]

- Fix declaration of test dependencies. [#9]

0.1.1 (2019-10-25)
==================

- Make plugin not crash if astropy is not installed. [#1]

0.1 (2019-10-25)
================

- Initial release.
